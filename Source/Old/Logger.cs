﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logging
{
    public class Logger : Helper
    {
        public const string Loggerversion = "2.0.2";

        public Logger(string filedirectory, string filename, bool StopforWarning = false, int Loglevel = 5)
            : base(filedirectory, filename, StopforWarning, Loglevel)
        {
        }

        public new string GetVersion()
        {
            return string.Format("Logging.DLL Versions:{0}Logger Version: {1}{0}Backend Version: {2}", Environment.NewLine, Loggerversion, base.GetVersion());
        }

        public bool Log(string strLine, params object[] Args)
        {
            return base.Log(string.Format(strLine, Args));
        }
        public bool LogWarning(string strLine, params object[] Args)
        {
            return base.LogWarning(string.Format(strLine, Args));
        }
        public bool LogError(string strLine, params object[] Args)
        {
            return base.LogError(string.Format(strLine, Args));
        }
        public bool LogCriticalError(int Errorcode, string strLine, params object[] Args)
        {
            return base.LogCriticalError(Errorcode, string.Format(strLine, Args));
        }
    }
}
