﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Logging
{
    public abstract class Helper
    {
        internal const string version = "2.0.3";
        internal string EmergencyLogPath = Path.GetTempPath() + "Logging_DLL_Critical_Error.log";


        internal bool Configured;
        internal string FileName;
        internal string FileDirectory;
        internal string FullFilePath;

        internal int loglevel;
        private int MaxLogLevel = 5;

        internal bool StopforWarning;

        internal Helper(string filedirectory, string filename, bool Stopforwarning, int Loglevel)
        {
            FileDirectory = filedirectory;
            FileName = filename;
            loglevel = Loglevel;
            StopforWarning = Stopforwarning;
            FullFilePath = FileDirectory + FileName;
            try
            {
                if (!File.Exists(FullFilePath))
                {
                    if (Directory.Exists(FileDirectory))
                    {
                        File.Create(FullFilePath).Close();
                    }
                    else
                    {
                        Directory.CreateDirectory(filedirectory);
                        if (Directory.Exists(FileDirectory))
                        {
                            File.Create(FullFilePath).Close();
                        }
                        else
                        {
                            Configured = false;
                            return;
                        }
                    }
                }
                Configured = true;
            }
            catch (Exception ex)
            {
                EmergencyLog("The following error occurs while trying to initialize the backend: {0}", ex.Message);
            }
        }
        internal string GetVersion()
        {
            return version;
        }
        internal void EmergencyLog(string Logline, params object[] Parameter)
        {
            try
            {
                WriteLine(String.Format(Logline, Parameter), EmergencyLogPath, true);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fatal error: {0}", ex.Message);
            }
        }
        internal bool WriteLine(string strLine, string path, bool ForceWrite = false)
        {
            try
            {
                if (IsConfigured() || ForceWrite)
                {
                    using (StreamWriter writer = new StreamWriter(path, true))
                    {
                        writer.WriteLine(strLine);
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can't write line see the error below: {0}{1}", Environment.NewLine, ex.Message);
                return false;
            }

        }
        internal virtual bool Log(string strLine, bool LogAnyway = false)
        {
            if (loglevel >= 3 || LogAnyway)
            {
                Console.WriteLine(strLine);
                if (MultinineLineCheck(strLine))
                    return true;
                WriteLine(string.Format("{0}: {1}", GetDateTime(), strLine), FullFilePath);
                return true;
            }
            return false;
        }
        internal virtual bool LogWarning(string strLine)
        {
            if (loglevel >= 2)
            {
                Console.WriteLine(strLine);
                if (MultinineLineCheck(strLine))
                    return true;
                WriteLine(string.Format("{0}: Warning: {1}", GetDateTime(), strLine), FullFilePath);
                return true;
            }
            return false;
        }
        internal virtual bool LogError(string strLine)
        {
            if (loglevel >= 1)
            {
                Console.WriteLine(strLine);
                if (MultinineLineCheck(strLine))
                    return true;
                WriteLine(string.Format("{0}: Error: {1}", GetDateTime(), strLine), FullFilePath);
                return true;
            }
            return false;
        }
        internal virtual bool LogCriticalError(int Exitcode, string strLine)
        {
            if (loglevel >= 0)
            {
                Console.WriteLine(strLine);
                if (MultinineLineCheck(strLine))
                    return true;
                WriteLine(string.Format("{0}: Critical Error: {1}", GetDateTime(), strLine), FullFilePath);
                WriteLine(string.Format("{0}: Exitcode: {1}", GetDateTime(), Exitcode.ToString()), FullFilePath);
                return true;
            }
            return false;
        }
        internal bool IsConfigured()
        {
            if (!Configured)
            {
                Console.WriteLine("Trying to use Logging.dll without correct Configuration. Please check the following file: {0}", EmergencyLogPath);
            }
            return Configured;
        }
        private string GetDateTime()
        {
            DateTime DT = DateTime.Now;
            return string.Format("{0} {1}", DT.ToString("d"), DT.ToString("T"));
        }
        private bool MultinineLineCheck(string Line)
        {
            if (Line.Contains("\r\n") || Line.Contains("\n"))
            {
                Line = CreateMutlilineString(Line);
                WriteLine(Line, FullFilePath);
                return true;
            }

            return false;
        }
        private string CreateMutlilineString(string Line)
        {
            string Head = string.Format("###{0}###", GetDateTime());
            string End = "";
            for (int i = 0; i < Head.Length; i++)
            {
                End += "#";
            }
            return string.Format("{0}{1}{0}{2}{0}{3}{0}", Environment.NewLine, Head, Line, End);
        }
        public virtual bool Delete()
        {
            try
            {
                File.Delete(FullFilePath);
                if (!File.Exists(FullFilePath))
                {
                    File.Create(FullFilePath).Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogCriticalError(1, ex.ToString());
            }

            return false;
        }
        public long GetSize()
        {
            FileInfo FI = new FileInfo(FullFilePath);
            return FI.Length;
        }

        public virtual string GetPath()
        {
            return FullFilePath;
        }

        public virtual void Space()
        {
            WriteLine(string.Empty, FullFilePath);
        }
        public virtual void Close()
        {
            Space();
        }

        public virtual void SetLogLevel(int NewLoglevel)
        {
            if (NewLoglevel > MaxLogLevel)
            {
                NewLoglevel = MaxLogLevel;
            }
            if (NewLoglevel < 0)
            {
                NewLoglevel = 0;
            }
            int OldLevel = loglevel;
            loglevel = NewLoglevel;
            Log(string.Format("Loglevel Changed from {0} to {1}", OldLevel, loglevel), true);

        }
        public string ReadLogFile()
        { 
            try 
	        {	        
		        if (File.Exists(FullFilePath))
                {
                    string ReturnText = string.Empty;
                    using (StreamReader reader = new StreamReader(FullFilePath))
                    {
                        string line = string.Empty;
                        while ((line = reader.ReadLine()) != null)
                        {
                            ReturnText += line + Environment.NewLine;
                        }
                    }
                    return ReturnText;
                }
	        }
	        catch (Exception ex)
	        {
                return "The following Error occured while trying to load the logfile\n" + ex.Message;
	        }

            return "UNKNOWN ERROR";
        }

        public string[] ReadAllLogLines()
        {
            try
            {
                if (File.Exists(FullFilePath))
                {
                    return File.ReadAllLines(FullFilePath);
                }
            }
            catch (Exception ex)
            {
                return new string[] { "The following Error occured while trying to load the logfile\n" + ex.Message };
            }
            return new string[] { "UNKNOWN ERROR" };
        }

        public virtual int LogLevel()
        {
            return loglevel;
        }

        public virtual int GetMaxLogLevel()
        {
            return MaxLogLevel;
        }
    }
}
