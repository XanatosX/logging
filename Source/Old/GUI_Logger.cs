﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;

namespace Logging
{
    public class GUI_Logger : Helper
    {
        public const string Loggerversion = "2.0.2";

        public GUI_Logger(string filedirectory, string filename, bool StopforWarning = false, int Loglevel = 5, bool UseModernStyle = true)
            : base(filedirectory, filename, StopforWarning, Loglevel)
        {
            if (UseModernStyle)
                Application.EnableVisualStyles();
        }

        public new string GetVersion()
        {
            return string.Format("Logging.DLL Versions:{0}GUI Logger Version: {1}{0}Backend Version: {2}", Environment.NewLine, Loggerversion, base.GetVersion());
        }

        public bool Log(string strMessageLine, params object[] strMessageLineArgs)
        {
            bool ReturnValue = base.Log(string.Format(strMessageLine, strMessageLineArgs));
            return ReturnValue;
        }
        public bool LogWindow(string strMessageLine, string strHeadLine, params object[] strMessageLineArgs)
        {
            bool ReturnValue = base.Log(string.Format(strMessageLine, strMessageLineArgs));
            ShowMessageWindow(strMessageLine, strMessageLineArgs, strHeadLine, MessageBoxIcon.None);
            return ReturnValue;
        }
        public bool LogWarning(string strMessageLine, string strHeadLine, params object[] strMessageLineArgs)
        {
            bool ReturnValue = base.LogWarning(string.Format(strMessageLine, strMessageLineArgs));
            ShowMessageWindow(strMessageLine, strMessageLineArgs, strHeadLine, MessageBoxIcon.Warning);
            return ReturnValue;
        }
        public bool LogError(string strMessageLine, string strHeadLine, params object[] strMessageLineArgs)
        {
            bool ReturnValue = base.LogError(string.Format(strMessageLine, strMessageLineArgs));
            ShowMessageWindow(strMessageLine, strMessageLineArgs, strHeadLine, MessageBoxIcon.Error);
            return ReturnValue;
        }
        public bool LogCriticalError(string strMessageLine, string strHeadLine, params object[] strMessageLineArgs)
        {
            bool ReturnValue = base.LogError(string.Format(strMessageLine, strMessageLineArgs));
            ShowMessageWindow(strMessageLine, strMessageLineArgs, strHeadLine, MessageBoxIcon.Error);
            return ReturnValue;
        }

        private void ShowMessageWindow(string strMainMessage, object[] strMainMessageArgs, string strHeadLine, MessageBoxIcon MessageBoxIcon)
        {
            MessageBox.Show(String.Format(strMainMessage, strMainMessageArgs), strHeadLine, MessageBoxButtons.OK, MessageBoxIcon);
        }
    }
}
