﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logging
{
    public class LogData
    {
        private LogLevel _loglevel;
        public LogLevel logLevel
        {
            get
            {
                return _loglevel;
            }
        }
        private bool _forceLog;
        public bool ForceWrite
        {
            get
            {
                return _forceLog;
            }
        }

        private string _rawLine;
        public string RawData
        {
            get
            {
                return _rawLine;
            }
        }

        public string LogLine
        {
            get
            {
                if (_multiLine)
                    return createMultilineString();
                else
                    return createBasicString();
            }
        }

        private bool _multiLine;
        public bool Multiline
        {
            get
            {
                return _multiLine;
            }
        }

        private DateTime _timeStamp;
        public DateTime TimeStamp
        {
            get
            {
                return _timeStamp;
            }
        }

        public LogData(string strLine, LogLevel loglevel, bool ForceWrite = false)
        {
            _timeStamp = DateTime.Now;
            _rawLine = strLine;
            _loglevel = loglevel;
            _forceLog = ForceWrite;
            _multiLine = IsMultiline();
        }

        private string DateString()
        {
            return _timeStamp.ToString("G");
        }

        private bool IsMultiline()
        {
            return (_rawLine.Contains("\r\n") || _rawLine.Contains("\n") || _rawLine.Contains(Environment.NewLine)) ? true : false;
        }

        private string createMultilineString()
        {
            string start = String.Format("### {0} - {1} ###", _loglevel.ToString(), DateString());
            string end = String.Empty;

            for (int i = 0; i < start.Length; i++)
            {
                end += "#";
            }

            return String.Format("{0}{1}{0}{2}{0}{3}{0}", Environment.NewLine, start, _rawLine, end);
        }

        private string createBasicString()
        {
            return String.Format("{0} {1}: {2}", DateString(), _loglevel.ToString(), _rawLine);
        }
    }
}
