﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Logging
{
    public enum LogLevel
    { 
        Unknown,
        Information,
        Warning,
        Error,
        Critical,
    }

    public class BasicLogger
    {
        protected Version _basicLoggerVersion = new Version(1,0,0,0);

        protected bool _configured;
        protected string _filename;
        protected string _folderName;
        protected string _fullPath
        {
            get
            {
                return _folderName + _filename;
            }
            
        }
        public string LogFile
        {
            get
            {
                return _fullPath;
            }
        }

        private string _backendErrorPath;

        public BasicLogger(string FileDirectory, string Filename)
        {
            _filename = Filename;
            _folderName = FileDirectory;
            _backendErrorPath = Path.GetTempPath() + "Logging_DLL_V_"+"VERSION"+"_Critical_Error.log";
            try
            {
                if (!File.Exists(_fullPath))
                {
                    if (!Directory.Exists(_folderName))
                        Directory.CreateDirectory(_folderName);
                    File.Create(_fullPath).Close();
                }
            }
            catch (Exception ex)
            {
                LogBackendError(ex.Message);
            }
            finally
            {
                _configured = File.Exists(_fullPath);
            }
        }

        private bool LogBackendError(string Logline, params object[] Parameter)
        {
            try
            {
                if (_configured)
                {
                    Console.WriteLine(String.Format(Logline, Parameter));
                    using (StreamWriter writer = new StreamWriter(_backendErrorPath, true))
                    {
                        writer.WriteLine(String.Format(Logline, Parameter));
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                LogBackendError(ex.Message);
                return false;
            }
        }

        public bool WriteLine(string line, LogLevel level, params object[] Args)
        {
            LogData data = new LogData(string.Format(line, Args), level);
            return WriteLine(data);
        }

        protected bool WriteLine(LogData data)
        {
            try
            {
                if (_configured)
                {
                    using (StreamWriter writer = new StreamWriter(_fullPath, true))
                    {
                        writer.WriteLine(data.LogLine);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                LogBackendError(ex.Message);
                return false;
            }
        }
    }
}
