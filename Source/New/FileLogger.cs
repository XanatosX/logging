﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logging
{
    public class FileLogger : BasicLogger
    {
        protected Version loggerversion = new Version(1, 0, 0, 0);
        public string Version
        {
            get
            {
                return GenerateVersion();
            }
        }

        public FileLogger(string FileDirectory, string Filename) 
            : base(FileDirectory, Filename)
        {
                
        }

        protected virtual string GenerateVersion()
        {
            return string.Format("Logging.DLL Versions:{0}File Logger Version: {1}{0}Backend Version: {2}", Environment.NewLine, loggerversion.ToString(), _basicLoggerVersion.ToString());
        }

        public virtual bool Log(string strLine, LogLevel loglevel, params object[] Args)
        {
            return WriteLine(strLine, loglevel, Args);
        }
    }
}
